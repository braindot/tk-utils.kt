# Tk-Utils.kt

Wrapper of the Toolkit in Kotlin, to use in Kotlin scripts

## Getting Started

Download links:

SSH clone URL: ssh://git@git.jetbrains.space/braindot/Tk-Utils.kt.git

HTTPS clone URL: https://git.jetbrains.space/braindot/Tk-Utils.kt.git



These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

What things you need to install the software and how to install them.

```
Examples
```

## Deployment

To be able to connect to Space, create the following file:

```properties
# In file gradle-local.properties
space_username="Your space username here"
space_password="Your space password here"
```

You can then run `./gradlew publish` to release a new version.

## Resources

Add links to external resources for this project, such as CI server, bug tracker, etc.
