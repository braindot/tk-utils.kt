FROM archlinux:latest
MAINTAINER braindot.net

RUN pacman -Sy --noconfirm bash zsh jre-openjdk jdk-openjdk && source /etc/profile
ENV JAVA_HOME /usr/lib/jvm/default
