//region Plugins & identity
plugins {
    kotlin("jvm") version "1.3.72"
    idea
    `maven-publish`
    id("net.saliman.properties") version "1.5.1"
    id("com.palantir.git-version") version "0.12.3"
}

val gitVersion: groovy.lang.Closure<*> by extra

group = "fr.braindot"
version = (gitVersion() as String).removePrefix("v")

repositories {
    mavenCentral()
}
//endregion

//region Dependencies
dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("test-junit"))
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}
//endregion

//region IDEA integration
idea {
    module {
        isDownloadJavadoc = true
    }
}
//endregion

//region Maven releases
publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = group.toString()
            artifactId = "tk-utils-kt"
            version = project.version.toString()

            from(components["java"])
        }
    }

    repositories {
        if (project.ext.has("space_username") && project.ext.has("space_password")) {
            maven {
                credentials {
                    username = project.ext.get("space_username").toString().trim('"')
                    password = project.ext.get("space_password").toString().trim('"')
                }
                url = uri("https://maven.pkg.jetbrains.space/braindot/p/tk/braindot-toolkit")
            }
        } else {
            println(
                """You are not identified for https://braindot.jetbrains.space.
To be able to publish to Space, create the 'space_username' and 'space_password' properties.
See README.md for more information."""
            )
        }
    }
}
//endregion
