package fr.braindot.toolkit.shell

/**
 * Wrapper around the UNIX shell Zsh.
 *
 * See also [zsh].
 */
object Zsh : SimpleShell(arrayOf("zsh", "-c"))

/**
 * Executes a command with Zsh.
 *
 * Wrapper around [Zsh.execute].
 */
fun zsh(command: String) = Zsh.execute(command)
