package fr.braindot.toolkit.shell

import java.io.InputStream
import java.util.stream.Collectors

/**
 * Represents the results of the execution of an external process.
 */
@Suppress("MemberVisibilityCanBePrivate")
class Command(private val process: Process) {

	//region Standard output
	/**
	 * Access to the standard output of this command.
	 * This [InputStream] can only be consumed once: subsequents calls to [stdoutText] or [stdoutTextOrThrow] will be empty.
	 */
	val stdout: InputStream = process.inputStream

	/**
	 * Reads the standard output of this command into a [String].
	 * The standard output can only be consumed once: once this method is called, [stdout] and [stdoutTextOrThrow] will be empty.
	 * However, this method can be called as many times as necessary (the results from the first call are memoized).
	 */
	val stdoutText: String by lazy { stdout.bufferedReader().lines().collect(Collectors.joining("\n")) }

	/**
	 * Reads the standard output of this process into a [String], or throws an exception that contains the error output.
	 * This will consume the standard output ([stdout] and [stdoutText])
	 * Note that the first call is blocking.
	 */
	val stdoutTextOrThrow: String by lazy {
		if (exitCode != 0) {
			val msg = StringBuilder("Command failed with code $exitCode, see details below:\n")

			if (stdoutText.isNotBlank()) msg.append(" -> Standard output:\n$stdoutText\n")
			else msg.append(" -> No standard output.\n")

			if (stderrText.isNotBlank()) msg.append(" -> Error output:\n$stderrText\n")
			else msg.append(" -> No error output.\n")

			throw RuntimeException(msg.toString())
		} else stdoutText
	}
	//endregion

	//region Error output
	/**
	 * Access to the error output of the command.
	 * This [InputStream] can only be consumed once: subsequent calls to [stderrText] will be empty.
	 */
	val stderr: InputStream = process.errorStream

	/**
	 * Reads the error output of this command into a [String].
	 * The error output can only be consumed once: once this method is called, [stderr] will be empty.
	 * However, this method can be called as many times as necessary (the results from the first call are memoized).
	 */
	val stderrText: String by lazy { stderr.bufferedReader().lines().collect(Collectors.joining("\n")) }
	//endregion

	/**
	 * The UNIX exit code of this command.
	 * Note that calling this method for the first time might block the current thread.
	 */
	val exitCode by lazy { process.waitFor() }
}

