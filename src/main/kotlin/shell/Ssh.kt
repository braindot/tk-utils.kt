package fr.braindot.toolkit.shell

/**
 * Connects to a remote server via SSH and executes a command.
 *
 * You can specify which [user] you want to connect with, which [host] you want to connect to, and which [remoteShell] you want to use.
 */
class Ssh(
	private val user: String?, private val host: String, private val remoteShell: SimpleShell
) : Shell {

	override fun execute(command: String): Command {
		return Shell.systemExecute("ssh",
			"${user?.let { "user@" } ?: ""}$host",
			"${remoteShell.shell.joinToString(" ")} '$command'")
	}

}
