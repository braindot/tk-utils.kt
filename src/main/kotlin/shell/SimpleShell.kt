package fr.braindot.toolkit.shell

/**
 * A simple implementation of a [Shell], that simply appends some arguments before the called script.
 *
 * For example, executing a Bash script can be done with:
 * ```shell
 * bash -c <your script>
 * ```
 */
abstract class SimpleShell(val shell: Array<String>) : Shell {

	override fun execute(command: String): Command {
		return Shell.systemExecute(*shell, command)
	}

	override fun toString() = shell.joinToString(" ")

}
