package fr.braindot.toolkit.shell

/**
 * Wrapper around the UNIX utility Bash, or "Bourne Again Shell".
 *
 * See [bash].
 */
object Bash : SimpleShell(arrayOf("bash", "-c"))

/**
 * Executes a command with Bash.
 *
 * Wrapper around [Bash.execute].
 */
fun bash(command: String) = Bash.execute(command)
