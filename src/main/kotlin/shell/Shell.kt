package fr.braindot.toolkit.shell

/**
 * Represents a UNIX shell, that can be used to execute commands on the underlying system.
 *
 * See [execute].
 */
interface Shell {

	/**
	 * Executes a command with the current shell.
	 */
	fun execute(command: String): Command

	companion object {

		/**
		 * Executes an arbitrary command with the default shell of the system.
		 */
		fun systemExecute(vararg command: String): Command {
			println("$ ${command.joinToString(" ") { "⟨$it⟩" }}")
			val processBuilder = ProcessBuilder().command(*command)

			processBuilder.redirectInput()

			return Command(processBuilder.start())
		}
	}
}
