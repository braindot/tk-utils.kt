
package fr.braindot.toolkit

import fr.braindot.toolkit.shell.Bash
import fr.braindot.toolkit.shell.Shell
import java.lang.IllegalStateException
import kotlin.system.exitProcess

class Print(private val toolkit: Toolkit) {

    private fun print(level: LogLevel, message: String, config: Config = Config(null)) {
        text(level.pattern.replace("%m", message))

        if (config.exitCode != null)
            exitProcess(config.exitCode)
    }

    /**
     * An error [message]. Will exit the program with [exitCode].
     */
    fun error(message: String, exitCode: Int = 1) {
        print(LogLevel.ERROR, message, Config(exitCode))
    }

    /**
     * A warning [message].
     */
    fun warn(message: String) {
        print(LogLevel.WARN, message)
    }

    /**
     * An information [message]. Will only be printed in [verbose mode][Toolkit.verbose].
     */
    fun debug(message: String) {
        if (toolkit.verbose)
            print(LogLevel.MINOR, message)
    }

    /**
     * A convenient alias on [debug].
     */
    fun info(message: String) = debug(message)

    /**
     * Displays an important [message].
     */
    fun important(message: String) {
        print(LogLevel.MAJOR, message)
    }

    /**
     * A convenient alias on [important].
     */
    fun emph(message: String) = important(message)

    /**
     * Prints an information [message].
     */
    fun text(message: String) {
        print(LogLevel.TEXT, message)
    }

    fun run(message: String, sudo: Boolean = false, shell: Shell = Bash) {
        print(if (sudo) LogLevel.RUN_ROOT else LogLevel.RUN, message)
        shell.execute(message)
    }

    data class Config(val exitCode: Int?)
}

enum class LogLevel(environmentVariable: String) {
    ERROR("TK_PROMPT_ERROR"),
    WARN("TK_PROMPT_WARN"),
    RUN("TK_PROMPT_RUN"),
    RUN_ROOT("TK_PROMPT_RUN_ROOT"),
    MAJOR("TK_PROMPT_MAJOR"),
    MINOR("TK_PROMPT_MINOR"),
    TEXT("TK_PROMPT_TEXT");

    val pattern = loadEnvOrDie(environmentVariable)

    private fun loadEnvOrDie(variable: String) = System.getenv(variable)
        ?: throw IllegalStateException("Could not find environment variable '$variable'...")
}
