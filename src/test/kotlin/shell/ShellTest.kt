package fr.braindot.toolkit.shell

import org.junit.Test
import kotlin.test.assertEquals

class ShellTest {

    @Test
    fun `Bash shell`() {
        assertEquals("true", Bash.execute("echo true").stdoutTextOrThrow)

        println(Bash.execute("bash --version | head -1").stdoutTextOrThrow)
    }

    @Test
    fun `Zsh shell`() {
        assertEquals("true", Zsh.execute("echo true").stdoutTextOrThrow)

        println(Zsh.execute("zsh --version").stdoutTextOrThrow)
    }

    /* Cannot run in a CI server
    @Test
    fun `SSH shell`() {
        assertEquals("true", Ssh(null, "localhost", Bash).execute("echo true").stdoutTextOrThrow)
    }
    */

}
